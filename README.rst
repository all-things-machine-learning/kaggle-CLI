Kaggle-CLI
=======================

Usage
------

Let's pretend you want to use the Kaggle Command Line Interface (Kaggle-CLI). 
First, pull the latest Kaggle-CLI docker image::

    $ docker pull registry.gitlab.com/all-things-machine-learning/kaggle-cli

Now run it with this command::

    $ docker run -it registry.gitlab.com/all-things-machine-learning/kaggle-cli

This will give you a bash prompt from which you can run the kg command.  You 
may wish to link a volume if going to download datasets.  
