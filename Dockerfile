FROM python:3.6-alpine
# docker run -it --rm alpine /bin/ash
# docker run -it --rm python:3.6-alpine /bin/ash


# Edit with mysql-client, postgresql-client, sqlite3, etc. for your needs.
# Or delete entirely if not needed.
# RUN apk --no-cache add postgresql-client
#RUN apk add --no-cache add gcc

RUN apk update && apk --no-cache add \
      libxml2 libxml2-dev libxslt-dev gcc libc-dev \
      libc-dev  # Resolves: could not find function xmlCheckVersion in
                # library libxml2. Is libxml2 installed?

WORKDIR /app
COPY requirements.txt /app
RUN pip install --no-cache-dir -r requirements.txt

#RUN adduser -D -u 1001 noroot

#USER noroot

#COPY . /usr/src/app_kaggle_cli

CMD ["/bin/sh"]
#ENTRYPOINT [""]
